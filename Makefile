CC := g++
BUILDDIR := build
TARGET := bin/program

CFLAGS := -Wall -Wextra -std=c++11
LFLAGS := -lX11 -lGL -lpthread -lpng -lstdc++fs

SOURCES := $(wildcard *.cpp)
OBJECTS := $(SOURCES:.cpp=.o)
DEP := $(OBJECTS:.o=.d)

$(TARGET): $(OBJECTS)
	@mkdir -p bin
	@echo " Linking..."
	@echo " $(CC) $^ -o $(TARGET) $(LFLAGS)"; $(CC) $^ -o $(TARGET) $(LFLAGS)

$(BUILDDIR)/%.o: %.cpp
	@mkdir -p $(dir $@)
	@echo " $(CC) $(CFLAGS) -MMD -c -o $@ $<"; $(CC) $(CFLAGS) -MMD -c -o $@ $<

-include $(DEP)

.PHONY: clean
clean:
	@echo " Cleaning...";
	@echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

.PHONY: debug
debug: CFLAGS += -g -DDEBUG
debug: $(TARGET)

.PHONY: release
release: CFLAGS += -O2
release: $(TARGET)
