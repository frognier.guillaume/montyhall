#include <iostream>
#include <random>
#include <string>
#include <vector>

#define OLC_PGE_APPLICATION
#include "olcPixelGameEngine.h"

static const size_t WIDTH = 2048;
static const size_t HEIGHT = 1080;

class cBox
{
public:
    cBox(short pnum)
    {
        if (sprite == nullptr) {
            sprite = new olc::Sprite("sprite.png");
        }
        num = pnum;
    }
    virtual void Draw(olc::PixelGameEngine *engine, bool shown)
    {
        int spacer = (WIDTH - 3 * 390) / 4;
        int offsetX = spacer * (1 + num) + 390 * num;
        if (shown) {
            int spritey = 0;
            if (prized) {
                spritey = 310;
            }
            engine->DrawPartialSprite(offsetX + 40, 140, sprite, 130, spritey, 310, 310);
        } else {
            engine->DrawPartialSprite(offsetX, 100, sprite, 0, 0, 130, 350);
            engine->DrawPartialSprite(offsetX + 130, 100, sprite, 0, 0, 130, 350);
            engine->DrawPartialSprite(offsetX + 260, 100, sprite, 0, 0, 130, 350);
        }
        if (choice2) {
            // engine->DrawRect(offsetX,100,390,350,olc::RED);
            engine->FillCircle(offsetX, 450, 50, olc::RED);
        } else if (choice1) {
            // engine->DrawRect(offsetX,100,390,350,olc::DARK_YELLOW);
            engine->FillCircle(offsetX, 450, 50, olc::MAGENTA);
        }
    }
    bool isPrized()
    {
        return prized;
    }
    void SetPrize()
    {
        prized = true;
    }
    void SetChoice1()
    {
        choice1 = true;
    }
    void SetChoice2()
    {
        choice2 = true;
    }
    void Reset()
    {
        choice1 = false;
        choice2 = false;
        prized = false;
    }

private:
    static olc::Sprite *sprite;
    short num;
    bool prized;
    bool choice1;
    bool choice2;
};

olc::Sprite *cBox::sprite = nullptr;

class MontyHall : public olc::PixelGameEngine
{
private:
    enum GAME_STATE {
        GS_WAIT,
        GS_RESET,
        GS_GENERATE,
        GS_SHOW_HIDDEN_3,
        GS_CHOICE_1,
        GS_SHOW_HIDDEN_2,
        GS_CHOICE_2,
        GS_SHOW_HIDDEN_0,
    } nGameState,
        nNextState;
    cBox *box[3];
    short prized;
    short choice1;
    short choice2;
    short shown;
    bool strat_change;
    float fChangeTime;
    float fCurrentTime;
    unsigned int win;
    unsigned int los;
    unsigned int tg;
    float winrate;
    bool pause;

public:
    MontyHall()
    {
        sAppName = "MontyHall";
    }

    ~MontyHall()
    {
        for (int i = 0; i < 3; i++) {
            box[i] = nullptr;
            delete box[i];
        }
    }

    virtual bool OnUserCreate()
    {
        nGameState = GS_RESET;
        nNextState = GS_RESET;
        for (int i = 0; i < 3; i++) {
            box[i] = new cBox(i);
        }
        strat_change = false;
        pause = false;
        fChangeTime = 5.0f;
        fCurrentTime = fChangeTime;
        win = 0;
        los = 0;
        tg = 0;
        winrate = 0.0f;
        return true;
    }

    virtual bool OnUserUpdate(float fElapsedTime)
    {
        // Handle events.
        if (GetKey(olc::Key::E).bPressed)
            strat_change = !strat_change;
        if (GetKey(olc::Key::SPACE).bPressed)
            pause = !pause;
        if (GetKey(olc::Key::R).bPressed) {
            win = 0;
            los = 0;
            tg = 0;
            winrate = 0.0f;
            fChangeTime = 5.0f;
            nGameState = GS_RESET;
        }
        if (GetKey(olc::Key::F).bPressed) {
            fChangeTime -= 0.5f;
            fCurrentTime -= 0.5f;
            if (fChangeTime < 0)
                fChangeTime = 0;
        }
        if (GetKey(olc::Key::S).bPressed) {
            fChangeTime += 0.5f;
            fCurrentTime += 0.5f;
        }
        // Reduce time.
        if (!pause) {
            fCurrentTime -= fElapsedTime;
            if (fCurrentTime < 0) {
                fCurrentTime += fChangeTime;
                nGameState = nNextState;
            }
        }
        switch (nGameState) {
        case GS_WAIT:
            break;
        case GS_RESET:
            Clear(olc::DARK_GREEN);
            for (int i = 0; i < 3; i++)
                box[i]->Reset();
            choice1 = 0;
            choice2 = 0;
            srand(time(NULL));
            shown = rand() % 3;
            // show score:
            DrawString(100, 500, "WIN:  " + std::to_string(win), olc::BLACK, 5);
            DrawString(100, 550, "LOS:  " + std::to_string(los), olc::BLACK, 5);
            DrawString(100, 600, "GAME: " + std::to_string(tg), olc::BLACK, 5);
            DrawString(1000, 500, "Win rate: " + std::to_string(winrate), olc::BLACK, 5);
            srand(time(NULL) + (int)(fCurrentTime * 1000));
            prized = rand() % 3;
            box[prized]->SetPrize();
            for (int i = 0; i < 3; i++) {
                box[i]->Draw(this, false);
            }
            nNextState = GS_CHOICE_1;
            break;
        case GS_CHOICE_1:
            srand(time(NULL) + (int)(fCurrentTime * 1000) + 1);
            choice1 = rand() % 3;
            box[choice1]->SetChoice1();
            box[choice1]->Draw(this, false);
            nNextState = GS_SHOW_HIDDEN_2;
            break;
        case GS_SHOW_HIDDEN_2:
            while (shown == prized || shown == choice1) {
                shown++;
                if (shown > 2)
                    shown = 0;
            }
            box[shown]->Draw(this, true);
            nNextState = GS_CHOICE_2;
            break;
        case GS_CHOICE_2:
            if (strat_change) {
                while (choice2 == choice1 || choice2 == shown)
                    choice2++;
            } else
                choice2 = choice1;
            box[choice2]->SetChoice2();
            box[choice2]->Draw(this, false);
            nNextState = GS_SHOW_HIDDEN_0;
            break;
        case GS_SHOW_HIDDEN_0:
            for (int i = 0; i < 3; i++) {
                if (i != shown)
                    box[i]->Draw(this, true);
            }
            nNextState = GS_RESET;
            // update score:
            FillRect(100, 500, WIDTH - 100, HEIGHT - 400, olc::DARK_GREEN);
            tg++;
            if (box[choice2]->isPrized())
                win++;
            else
                los++;
            winrate = (float)win / (float)tg;
            // show score:
            DrawString(100, 500, "WIN:  " + std::to_string(win), olc::BLACK, 5);
            DrawString(100, 550, "LOS:  " + std::to_string(los), olc::BLACK, 5);
            DrawString(100, 600, "GAME: " + std::to_string(tg), olc::BLACK, 5);
            DrawString(1000, 500, "Win rate: " + std::to_string(winrate), olc::BLACK, 5);
            break;
        default:
            break;
        }
        nGameState = GS_WAIT;
        return true;
    }
};

int main()
{
    MontyHall game;
    if (game.Construct(WIDTH, HEIGHT, 4, 4))
        game.Start();
    return 0;
}
