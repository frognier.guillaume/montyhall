# MontyHall

[Monty Hall problem](https://en.wikipedia.org/wiki/Monty_Hall_problem)

## Keyboard input

- <kbd>space</kbd>: pause
- <kbd>R</kbd>: reset
- <kbd>F</kbd>: go faster
- <kbd>S</kbd>: go slower

## Tools needed

- GNU compiler g++
- CMake or GNU Makefile

## Libraries needed

### Linux

- X11
- OpenGL
- Threads
- PNG

### Windows

- user32
- gdi32
- opengl32
- gdiplus
- Shlwapi
- stdc++fs

## Build

- using CMAKE:

at root level: 

```
mkdir build
cd build
cmake ..
make
```

- using Makefile

at root level: 

```
make
```

launch with:
```
./bin/program
```
